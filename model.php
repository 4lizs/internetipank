<?php
$host   = "localhost";
$user   = "test";
$pass   = "t3st3r123";
$db     = "test";
$prefix = "ltellisk";

$link = new mysqli($host, $user, $pass, $db);
if ($link->connect_errno) {
    printf("Error: %s", $link->connect_error);
    exit();
}
if (!$link->query('SET CHARACTER SET UTF8')) {
    printf('Error: %s', $link->error);
    exit();
}

function model_load()
{
    global $link, $prefix;
    
    $id = $_SESSION['id'];
    
    $query = "SELECT 	tehingud.Id AS Id,
    					kasutaja_maksja.kasutajanimi AS Maksja,
						kasutaja_saaja.kasutajanimi AS Saaja,
    					tehingud.Summa AS Summa
    			FROM {$prefix}__tehingud AS tehingud 
    			JOIN {$prefix}__kasutajad AS kasutaja_maksja ON tehingud.Maksja = kasutaja_maksja.Id
    			JOIN {$prefix}__kasutajad AS kasutaja_saaja ON tehingud.Saaja = kasutaja_saaja.Id
    			WHERE tehingud.Maksja = $id OR tehingud.Saaja = $id
    			ORDER By Id ASC";
    
    $result = $link->query($query);
    if (!$result) {
        printf('Error: %s', $link->error);
        exit();
    }
    
    $rows = array();
    while ($row = $result->fetch_array()) {
        $rows[] = $row;
    }
    $result->close();
    
    return $rows;
}
// Inserts payment to databse and re-calculates the saldo for both parties after the transaction
function model_pay($maksja, $saaja, $summa)
{
    global $link, $prefix;
    $maksja = $link->real_escape_string($maksja);
    $saaja  = $link->real_escape_string($saaja);
    $summa  = $link->real_escape_string($summa);
    
    $query = "INSERT INTO {$prefix}__tehingud (Maksja, Saaja, Summa)
            VALUES ('$maksja', '$saaja', '$summa')";
    
    $result = $link->query($query);
    if (!$result) {
        printf('Error: %s "%s"', $link->error, $query);
        exit();
    }
    $id = $link->insert_id;
    
    // Updating the balance after a transaction
    
    $saldo_maksja = model_load_saldo($maksja);
    $saldo_saaja  = model_load_saldo($saaja);
    
    $query2  = "UPDATE {$prefix}__kasutajad
    	SET Kontoseis = CASE
        WHEN Id = $maksja THEN ($saldo_maksja - $summa)
        WHEN Id = $saaja THEN ($saldo_saaja + $summa)
    	END
    	WHERE Id IN ($maksja, $saaja)
    ";
    $result2 = $link->query($query2);
    if (!$result2) {
        printf('Error: %s "%s"', $link->error, $query);
        exit();
    }
    
    
    return $id;
}

function model_add_user($kasutajanimi, $parool)
{
    global $prefix, $link;
    
    $hash = password_hash($parool, PASSWORD_DEFAULT);
    
    $kasutajanimi = $link->real_escape_string($kasutajanimi);
    $hash         = $link->real_escape_string($hash);
    
    $query = "INSERT INTO {$prefix}__kasutajad (Kasutajanimi, Parool) VALUES ('$kasutajanimi','$hash')";
    
    $result = $link->query($query);
    if (!$result) {
        printf('Error: %s', $link->error);
        exit();
    }
    
    $id = $link->insert_id;
    return id;
}

function model_get_user($kasutajanimi, $parool)
{
    global $link, $prefix;
    
    $kasutajanimi = $link->real_escape_string($kasutajanimi);
    
    $query = "SELECT Id, Parool FROM {$prefix}__kasutajad WHERE Kasutajanimi='$kasutajanimi' LIMIT 1";
    
    $result = $link->query($query);
    if (!$result) {
        printf('Error: %s', $link->error);
        exit();
    }
    
    $kasutaja = $result->fetch_array();
    if (!$kasutaja) {
        return false;
    }
    
    $check_user = password_verify($parool, $kasutaja['Parool']);
    if ($check_user) {
        return $kasutaja['Id'];
    }
    return false;
}
// This function gets us all the users that we can make the payment.
function saaja_model_load()
{
    global $link, $prefix;
    
    $user = $_SESSION['user'];
    
    $query = "SELECT * FROM {$prefix}__kasutajad WHERE Kasutajanimi != '$user' ORDER BY Kasutajanimi ASC";
    
    $result = $link->query($query);
    if (!$result) {
        printf('Error: %s', $link->error);
        exit();
    }
    
    $rows = array();
    while ($row = $result->fetch_array()) {
        $rows[] = $row;
    }
    $result->close();
    
    return $rows;
}
// This function loads the saldo of given user. It takes ID as input.
function model_load_saldo($id)
{
    global $prefix, $link;
    
    $query = " SELECT Kontoseis FROM {$prefix}__kasutajad WHERE Id = $id LIMIT 1";
    
    $result = $link->query($query); // Performs a query against the database
    if (!$result) {
        printf('Error: %s', $link->error);
        exit();
    }
    
    $saldo = $result->fetch_array();
    $result->close();
    
    return $saldo['Kontoseis'];
}
?>

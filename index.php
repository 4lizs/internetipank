<?php

session_start();

// laeme sisse funktsioonid andmete laadimiseks ja salvestamiseks
require 'model.php';

// laeme funktsioonid andmete manipuleerimiseks
require 'controller.php';

// POST ruuter
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = false;

    // valime tegevuse vastavalt <input name=action> elemendi väärtusele
    switch ($_POST['action']) {

        case 'maksa':
            $maksja = intval($_SESSION['id']);
            $saaja = intval($_POST['saaja']);
            $summa = floatval($_POST['summa']);

            if ( model_load_saldo($maksja) < $summa){
            	header('Location: '.$_SERVER['PHP_SELF'].'?view=nofunds');
        	exit;
            }
            if ($summa <= 0){
            	header('Location: '.$_SERVER['PHP_SELF'].'?view=pank');
            }

            $result = controller_pay($maksja, $saaja, $summa);

            break;

        case 'login':
            $kasutajanimi = $_POST['kasutajanimi'];
            $parool = $_POST['parool'];
            $result = controller_login($kasutajanimi, $parool);
        
            break;

        case 'logout':
            $result = controller_logout();
            break;

        case 'register':
        	$kasutajanimi = $_POST['kasutajanimi'];
        	$parool = $_POST['parool'];
        	$parool2 = $_POST['parool2'];
        	$result = controller_add_user($kasutajanimi, $parool, $parool2);
        	break;    
    }

    if ($result) {
        header('Location: '.$_SERVER['PHP_SELF']);
    } else {
        echo 'Viga!';
    }

    exit;
}

$view = empty($_GET['view']) ? 'pank' : $_GET['view'];

switch ($view) {

    case 'pank':
        check_login();
        require 'view_pank.php';
        break;
    case 'login':
        require 'view_login.php';
        break;
    case 'register':
    	require 'view_reg.php';
    	break;    
    case 'nofunds' :
    	require 'view_nofunds.php';
    	break;	
    default:
        echo 'Viga!';
}

function check_login()
{
    if (!controller_user()) {
        header('Location: '.$_SERVER['PHP_SELF'].'?view=login');
        exit;
    }
}

<!doctype HTML>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Internetipank</title>
      <!-- Bootstrap -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/styles.css">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="container">
         <nav class="navbar navbar-default">
            <div class="container-fluid">
               <div class="navbar-header">
                  <a class="navbar-brand" href="<?= htmlentities($_SERVER['PHP_SELF']) ?>">Internetipank</a>
               </div>
               <ul class="nav navbar-nav navbar-right">
                  <li>
                     <form  method="post" action="<?= $_SERVER['PHP_SELF']?>">
                        <input type="hidden" name="action" value="logout">
                        <button class="navbar-brand" id="logout" type="submit"><span class="glyphicon glyphicon-log-out"></span></button>
                     </form>
                  </li>
               </ul>
            </div>
         </nav>
      </div>
      <div class="container">
         <form id="lisa-vorm" method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
            <input type="hidden" name="action" value="maksa">
            <table class="table table-hover">
               <tr>
                  <td>Saaja</td>
                  <td>
                     <select class="maksmine" name="saaja">
                        <option value=""> -- Vali nimekirjast -- </option>
                        <?php foreach (saaja_model_load() as $rida): ?>
                        <option value="<?= $rida['Id']; ?>">
                           <?= htmlspecialchars($rida['Kasutajanimi']); ?>
                        </option>
                        <?php endforeach; ?>
                     </select>
                  </td>
               </tr>
               <tr>
                  <td>Summa</td>
                  <td><input class="maksmine" step="0.01" type="number" name="summa" id="summa" value=""></td>
               </tr>
            </table>
            <p> <button type="submit">Maksa</button> </p>
         </form>
         <h2>Kontoseis</h2>
         <table class="table table-hover">
            <thead>
               <tr>
                  <th>Kasutajanimi</th>
                  <th>Kontojääk</th>
               </tr>
            </thead>
            <tbody>
               <td> <?= $_SESSION['user']; ?> </td>
               <td> <?= model_load_saldo($_SESSION['id']); ?> €</td>
            </tbody>
         </table>
         <h2>Tehingud</h2>
         <table id="tehingud" class="table table-hover">
            <thead>
               <tr>
                  <th>Maksja</th>
                  <th>Saaja</th>
                  <th>Summa</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  // väljastame tsükli abil ükshaaval kõik salvestatud read
                  
                  // tsükli algus
                  
                  
                  foreach (model_load() as $rida):
                  
                  ?>
               <?php if( htmlspecialchars($rida['Maksja']) == $_SESSION['user'] ) : ?>
               <tr>
                  <td> <?= htmlspecialchars($rida['Maksja']) ?> </td>
                  <td> <?= htmlspecialchars($rida['Saaja']) ?></td>
                  <td>- <?= htmlspecialchars($rida['Summa']) ?> €   
                  </td>
               </tr>
               <?php elseif ( htmlspecialchars($rida['Saaja']) == $_SESSION['user'] ) : ?>
               <tr>
                  <td> <?= htmlspecialchars($rida['Maksja']) ?> </td>
                  <td> <?= htmlspecialchars($rida['Saaja']) ?></td>
                  <td>+ <?= htmlspecialchars($rida['Summa']) ?> €        
                  </td>
               </tr>
               <?php endif; ?>  
               <?php
                  endforeach;
                  // ts?kli l?pp
                  ?>
      </div>
      </tbody>
      </table>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>
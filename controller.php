<?php

function controller_pay($maksja, $saaja, $summa)
{
    if ($maksja == '' || $saaja == 0 || $summa <= 0) {
        return false;
    }
    
    return model_pay($maksja, $saaja, $summa);
}

function controller_user()
{
    if (empty($_SESSION['user'])) {
        return false;
    }
    
    return $_SESSION['user'];
}

function controller_login($kasutajanimi, $parool)
{
    if ($kasutajanimi == '' || $parool == '') {
        return false;
    }
    
    $id = model_get_user($kasutajanimi, $parool);
    
    if (!$id) {
        return false;
    }
    
    session_regenerate_id();
    $_SESSION['user'] = $kasutajanimi;
    $_SESSION['id']   = $id;
    
    return $id;
}

function controller_logout()
{
    session_destroy();
    
    return true;
}

function controller_add_user($kasutajanimi, $parool, $parool2)
{
    
    if ($parool != $parool2 || $kasutajanimi == '' || $parool == '') {
        return false;
    }
    return model_add_user($kasutajanimi, $parool);
}
